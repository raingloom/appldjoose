#!/bin/lua
local M = {}

function M.escape_value(x)
	--FIXME
	return '"'..x..'"'
end

function M.escape_content(x)
	--FIXME
	return x
end

function M.put_attr_one(write,attribute,value)
	write(' ', attribute, '=', M.escape_value(value))
end

function M.put_attr_many(write,attribute,values)
	local l = #values
	local put = M.put_attr_one
	for i = 1, l-1 do
		put(write,attribute,values[i],' ')
	end
	if l > 0 then
		put(write,attribute,values[i])
	end
end

function M.new(env,write)
	local stack, sn = {}, 1
	return setmetatable({},{__index = function(self,key)
		local env_val = env[key]
		if env_val == nil then
			local tag = key
			
			write('<',tag)
			
			local attribute
			
			local ret = setmetatable({},{
				__index = function(self,key)
					if attribute then
						M.put_attr_one(write,'class',attribute)
						attribute = nil
					end
					if type(key) == 'string' then
						attribute = key
						return self
					else
						return nil
					end
				end,
				
				__call = function(self,x,y)
					if attribute then
						if self == x then
							--a:key 'value'
							--a:key {'v1', 'v2'}
							M.put_attr_any(write,attribute,y)
						else
							M.put_child(write,self,x)
						end
					else
						M.write_children(write,self,x)
					end
					return self
				end,
			})
			
			return ret
		else
			return env_val
		end
	end})
end

local _ENV = M.new(_ENV,io.write)

io.stdout:setvbuf'no'
io.stderr:setvbuf'no'

a
.a_css_class
:class 'another_class'
:class { 'one-more-class', 'and-one-more' }
:id 'bar' :href "https://lua.org/" {
	h1 "Made with Lua"
}

div {
	h1 'level-1',
	div {
		h2 'level-2',
		div {
			h3 'level-3'
		}
	}
}

ol {function()
	for i = 1, 10 do
		il(i)
	end
end}

ul {function()
	for word in ([[Hazelnut, Mystify, Cuttlefish, Lark, Lurk, Robert, Anglican, Pheromone, Halter top, Marmalade, Hardware, Laser, Pepper, Release, Kneecap, Falafel, Period, Chaste, Chased, Leggings, Wool, Sweater, Heartbeat, Heartbeat, Heart, Beat, Heart, Beat, Beat, Beat, Beat, Beat.]]):gmatch('%w+') do
		li{word}
	end
end}

return M
